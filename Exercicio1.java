import java.util.Scanner;

public class Exercicio1 {

	public static void main(String[] args) {
	Scanner input=new Scanner(System.in);
	int IdadeH[] = new int [5];
	int IdadeM[] = new int [5];
	double MediaH=0, MediaM=0;
	int CountH=0, CountM=0;
	System.out.println("Preencha as idades dos Homens: ");
	Idade(IdadeH);
	System.out.println("Preencha as idades das Mulheres: ");
	Idade(IdadeM);
	MediaH = Media(IdadeH, CountH);
	MediaM = Media(IdadeM, CountM);
	for (int i = 0; i < IdadeH.length; i++) {
		if (IdadeH[i]>MediaM) {
			CountH++;
		}
		if (IdadeM[i]>MediaH) {
			CountM++;
		}
	}
	System.out.println("Resultado:");
	System.out.println("Quantidade de homens com idade acima da media de mulheres: "+CountH);
	System.out.println("Quantidade de mulheres com idade acima da media de homens: "+CountM);
	}
	public static void Idade(int VetorI[]) {
		Scanner input=new Scanner(System.in);
		for (int i = 0; i < VetorI.length; i++) {
			System.out.println("informe a idade: ");
			VetorI[i] = input.nextInt();
		}
	}
	public static double Media(int VetorM[], double Count){
		int Soma=0;
		double Media;
		for (int i = 0; i < VetorM.length; i++) {
			Soma += VetorM[i];
		}
		Media = Soma / VetorM.length;
		return Media;
	}
}
